cmake_minimum_required(VERSION 3.16.0)

project(babymeter VERSION 3.1.0 LANGUAGES CXX)

# Options
option(DEBUG "Build with debugging messages and tools" ON)
if (DEBUG)
	add_definitions(-DDEBUG)
endif()
if (WIN32)
	option(WITH_DAHENG "Build with support for Daheng devices using GXiAPI" ON)
	if (WITH_DAHENG)
		add_definitions(-DDAHENG)
		set(GxIAPI_INCLUDE_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/videobackend/daheng/include")
		set(GxIAPI_LIBS
			${CMAKE_CURRENT_SOURCE_DIR}/videobackend/daheng/lib/GxIAPIx64.lib
			${CMAKE_CURRENT_SOURCE_DIR}/videobackend/daheng/lib/DxImageProc.lib
		)
	endif()
endif()

# C++
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -s")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -s")

# Windows (static compilation)
if (WIN32)
	set(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
	set(CMAKE_EXE_LINKER_FLAGS "-static")
endif()

# Qt
find_package(Qt5 5.15 COMPONENTS Core Gui Widgets Svg REQUIRED)

# OpenCV
if (WIN32)
	# use included pre-compiled static libraries
	set(OpenCV_STATIC ON)
	find_package(OpenCV REQUIRED PATHS "opencv/win32" NO_DEFAULT_PATH)
	# XXX HACK XXX
	# libjpeg-turbo doesn't get linked for some reason
	# maybe this is just for this version, maybe I configured something wrong
	# ... probably the latter ;)
	set(OpenCV_HACK "${CMAKE_CURRENT_SOURCE_DIR}/opencv/win32/x64/mingw/staticlib/liblibjpeg-turbo.a")
else()
	find_package(OpenCV REQUIRED)
endif()

# Files
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include
	${OpenCV_INCLUDE_DIRS}
	${GxIAPI_INCLUDE_DIRS}
)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

#set(CMAKE_AUTOMOC ON)
qt_wrap_cpp(HEADERS
	include/videobackend/daheng.h
	include/videobackend/opencv.h
	include/videodevice.h
	include/opencv.h
	include/imageview.h
	include/segmentitemdelegate.h
	include/chessboard.h
	include/calib.h
	include/settings.h
	include/restore.h
	include/main.h
)

#set(CMAKE_AUTORCC ON)
if (Qt5_VERSION VERSION_GREATER_EQUAL 6.0.0)
	qt_add_resources(RESOURCES gui/resources.qrc)
else()
	qt5_add_resources(RESOURCES gui/resources.qrc)
endif()

#set(CMAKE_AUTOUIC ON)
qt_wrap_ui(UI_HEADERS
	gui/calibsetup.ui
	gui/calibcapture.ui
	gui/calibreview.ui
	gui/chessboard.ui
	gui/about.ui
	gui/settings.ui
	gui/restore.ui
	gui/main.ui
)

add_executable(${PROJECT_NAME}
	${HEADERS}
	${RESOURCES}
	${UI_HEADERS}
	videobackend/daheng.cpp
	videobackend/opencv.cpp
	videodevice.cpp
	opencv.cpp
	imageview.cpp
	segmentitemdelegate.cpp
	chessboard.cpp
	calib.cpp
	settings.cpp
	restore.cpp
	main.cpp
)

if (NOT DEBUG)
	# prevent console window from showing up on win32 platforms
	if (WIN32)
		set_property(TARGET ${PROJECT_NAME} PROPERTY WIN32_EXECUTABLE ON)
	endif()
endif()

target_include_directories(${PROJECT_NAME} PRIVATE include)
target_link_libraries(${PROJECT_NAME} PRIVATE
	Qt::Core
	Qt::Gui
	Qt::Widgets
	Qt::Svg
	${OpenCV_LIBS}
	${OpenCV_HACK}
	${GxIAPI_LIBS}
)
qt_import_plugins(${PROJECT_NAME}
	INCLUDE Qt::QSvgPlugin
)
