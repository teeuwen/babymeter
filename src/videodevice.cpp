/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <videodevice.h>
#include <videobackend/daheng.h>
#include <videobackend/opencv.h>

/* list of backends to be initialized (when program is started up) */
static VideoDeviceManager *backends[] = {
#ifdef DAHENG
	new DahengVideoDeviceManager,
#endif
	new OpenCVVideoDeviceManager
};

/*
 * static video devices (not enumerated with videodevices())
 * this is used to display static images (for example when a sampleset is loaded in)
 */
static StaticVideoDevice static_left, static_right;

/* retrieve a copy of the last frame */
cv::Mat AcquisitionThread::getFrame()
{
	mutex.lock();
	cv::Mat copy = frame.clone();
	mutex.unlock();

	return copy;
}

/* retrieve a list of all video devices from all backends */
QList<VideoDevice *> videodevices()
{
	QList<VideoDevice *> devices;

	for (unsigned int i = 0; i < sizeof(backends) / sizeof(void *); i++)
		devices.append(backends[i]->enumerate());

	return devices;
}

/* return the currently active left video device */
VideoDevice *left()
{
	QSettings settings;
	QList<VideoDevice *> devices;
	int n;

	/* attempt to retrieve video device index from settings */
	if (!settings.contains("camera/left/index"))
		return nullptr;
	n = settings.value("camera/left/index").toInt();

	/* check if device index is within bounds of the current device list */
	devices = videodevices();
	if (n > devices.size() - 1)
		return nullptr;

	return devices[n];
}

/* return the currently active right video device */
VideoDevice *right()
{
	QSettings settings;
	QList<VideoDevice *> devices;
	int n;

	/* attempt to retrieve video device index from settings */
	if (!settings.contains("camera/right/index"))
		return nullptr;
	n = settings.value("camera/right/index").toInt();

	/* check if device index is within bounds of the current device list */
	devices = videodevices();
	if (n > devices.size() - 1)
		return nullptr;

	return devices[n];
}

/* set the vdevs to StaticVideoDevice and load in 2 images for each device respectively */
void loadimgs(VideoDevice *&leftvdev, cv::Mat &leftsrc, VideoDevice *&rightvdev, cv::Mat &rightsrc)
{
	static_left.load(leftsrc);
	static_right.load(rightsrc);

	leftvdev = (VideoDevice *) &static_left;
	rightvdev = (VideoDevice *) &static_right;
}
