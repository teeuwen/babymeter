/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <videodevice.h>

class OpenCVAcquisitionThread : public AcquisitionThread
{
	Q_OBJECT

private:
	cv::VideoCapture videocap;

public:
	explicit OpenCVAcquisitionThread(QObject *parent = 0) {}

	void setVideoCapture(cv::VideoCapture cap)
	{
		videocap = cap;
	}

protected:
	void run() override
	{
		/* TODO handle error when device is suddenly closed */
		/* while (surfacecount && videocap.isOpened()) */
		while (surfacecount) {
			mutex.lock();
			videocap >> frame;
			mutex.unlock();
		}
	}
};

class OpenCVVideoDevice : VideoDevice
{
	Q_OBJECT

private:
	cv::VideoCapture videocap;
	int index;

public:
	OpenCVVideoDevice(int n);
	~OpenCVVideoDevice();

	int open();
	void close();

	int startAcq();
	void stopAcq();

	bool isOpen();
	void loadFrame();
};

class OpenCVVideoDeviceManager : public VideoDeviceManager
{
private:
	QList<VideoDevice *> devices;

public:
	OpenCVVideoDeviceManager();

	QList<VideoDevice *> enumerate();
};
