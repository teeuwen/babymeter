/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <imageview.h>
#include <opencv.h>

#include <QLabel>
#include <QList>
#include <QMutex>
#include <QThread>
#include <QTimer>

/* default global refreshrate in ms */
#define VIDEODEVICE_REFRESHRATE 20

/* refreshrate timer */
static QTimer refreshtimer;

class AcquisitionThread : public QThread
{
	Q_OBJECT

protected:
	QMutex mutex;
	cv::Mat frame;

public:
	/* the current number of surfaces this thread is drawing to */
	volatile int surfacecount {0};

	cv::Mat getFrame();
};

class VideoDevice : public QObject
{
	Q_OBJECT
private:
	QMap<void *, QMetaObject::Connection> connections;

protected:
	/* acquisition thread (optional) */
	AcquisitionThread *acqthread {nullptr};

	/* last frame pulled from the device */
	cv::Mat image;

public:
	/*
	 * TODO implement uuid
	 * The idea of the UUID is that VideoDevices are identifiable after a
	 * re-enumeration. Unfortunately, I haven't found a way yet to retrieve
	 * a device-bound value using the VideoCapture framework
	 */

	/*
	 * Unique identifier to remember the VideoDev by internally.
	 * It is implementation defined how this is calculated (it must be
	 * consistent though).
	 * */
	/* QUuid uuid; */

	/* video device name (shown in the GUI) */
	QString name;

	/*
	 * Open the video device
	 * returns 0 on success, -1 or lower on failure
	 */
	virtual int open() = 0;

	/* close the video device */
	virtual void close() = 0;

	/* start acquisition */
	virtual int startAcq() = 0;

	/* stop acquisition */
	virtual void stopAcq() = 0;

	/* check if the video device is opened */
	virtual bool isOpen() = 0;

	/* load one frame into the image buffer */
	virtual void loadFrame() = 0;

	/* retrieve the last videoframe from the videostream */
	cv::Mat getFrame()
	{
		return image;
	}

	/*
	 * Start the video stream and write the image to a surface
	 * `surface` is a QLabel or ImageView to write to
	 * if `single` only one frame will be written to `surface`
	 */
	/* TODO return int (on error) */
	template <typename ImageSurface>
	void stream(ImageSurface *surface, bool scale, bool single)
	{
		if (!isOpen() || !surface)
			return;

		/* start acquisition (if not already started) */
		if (!acqthread)
			if (startAcq() < 0)
				return;

		if (acqthread)
			acqthread->surfacecount++;

		/* TODO check if not already streaming to this same surface */

		if (single) {
			loadFrame();
			if (image.empty())
				return;

			if (scale)
				surface->setPixmap(mattopixmap(image).scaled(surface->size(), Qt::KeepAspectRatio));
			else
				surface->setPixmap(mattopixmap(image));

			return;
		}

		connections.insert(surface, connect(&refreshtimer, &QTimer::timeout, this,
		[this, surface, scale]()
		{
			loadFrame();
			updateSurface(surface, scale);
		}));
		refreshtimer.start(VIDEODEVICE_REFRESHRATE);
	}

	/* stop acquiring frames and rendering to all surfaces */
	void stop()
	{
		/* disconnect all surfaces */
		if (!connections.empty()) {
			foreach (QMetaObject::Connection conn, connections.values()) {
				disconnect(conn);
			}
		}
		refreshtimer.stop();

		/* stop acquisition */
		stopAcq();
	}

	/* update the surface that this device is streaming to */
	template <typename ImageSurface>
	void updateSurface(ImageSurface *surface, bool scale)
	{
		if (image.empty() || !surface)
			return;

		if (scale)
			surface->setPixmap(mattopixmap(image).scaled(surface->size(), Qt::KeepAspectRatio));
		else
			surface->setPixmap(mattopixmap(image));
	}
};

class VideoDeviceManager
{
public:
	virtual QList<VideoDevice *> enumerate() = 0;
};

/*
 * this is kind of a HACK (read: limitation of the VideoDevice framework) to
 * allow loading in images
 */
class StaticVideoDevice : VideoDevice
{
	Q_OBJECT

public:
	StaticVideoDevice() {}

	int open() { return 0; }
	void close() {}

	int startAcq() { return 0; }
	void stopAcq() {}

	void load(cv::Mat &source) {
		image = source;
	}

	bool isOpen() { return true; }
	void loadFrame() {}
};

QList<VideoDevice *> videodevices();
VideoDevice *left();
VideoDevice *right();

void loadimgs(VideoDevice *&leftvdev, cv::Mat &leftsrc, VideoDevice *&rightvdev, cv::Mat &rightsrc);
