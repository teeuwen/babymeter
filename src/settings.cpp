/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <settings.h>

#include <calib.h>

#include <QFileDialog>
#include <QGraphicsColorizeEffect>
#include <QStandardItemModel>

About::About(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);

	ui.logo1->setPixmap(QIcon(":/res/logo.svg").pixmap(QSize(100, 100)));
	ui.logo2->setPixmap(QIcon(":/res/logo2.svg").pixmap(QSize(100, 100)));
}

Settings::Settings(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	/* TODO disallow selecting same camera for both left and right */

	connect(ui.left_select, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
	[this](int i)
	{
		int res;

		/* check if the new selection is not the same one */
		if (i > 1 && vdev_left == videodevices()[i - 2])
			return;

		/* close old device (if not selected by the right dropdown) */
		if (vdev_left && vdev_left != vdev_right)
			vdev_left->close();

		/* invalid index */
		if (i < 2)
			return;

		/* attempt to open */
		vdev_left = videodevices()[i - 2];
		if ((res = vdev_left->open()) < 0) {
			settings.remove("camera/left");
			ui.left->setText(tr("unable to open camera: error ") + QString::number(res));

			return;
		}

		settings.setValue("camera/left/index", i - 2);

		vdev_left->stream(ui.left, true, false);

#ifdef W_OS_WIN32
		/* FIXME */
		/* const int backend = vdev_left.get(CAP_PROP_BACKEND);
		ui.left_properties->setVisible((backend == CAP_DSHOW));
		ui.left_resolution->setVisible((backend == CAP_DSHOW)); */
#endif
	});
	connect(ui.left_properties, &QAbstractButton::clicked, this,
	[=]()
	{
		/* FIXME */
		/* cap_left.set(CAP_PROP_SETTINGS, 0); */
	});

	connect(ui.right_select, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
	[this](int i)
	{
		int res;

		/* check if the new selection is not the same one */
		if (i > 1 && vdev_right == videodevices()[i - 2])
			return;

		/* close old device (if not selected by the left dropdown) */
		if (vdev_right && vdev_right != vdev_left)
			vdev_right->close();

		/* invalid index */
		if (i < 2)
			return;

		/* attempt to open */
		vdev_right = videodevices()[i - 2];
		if ((res = vdev_right->open()) < 0) {
			settings.remove("camera/right");
			ui.right->setText(tr("unable to open camera: error ") + QString::number(res));

			return;
		}

		settings.setValue("camera/right/index", i - 2);

		vdev_right->stream(ui.right, true, false);

#ifdef W_OS_WIN32
		/* FIXME */
		/* const int backend = vdev_right.get(CAP_PROP_BACKEND);
		ui.right_properties->setVisible((backend == CAP_DSHOW));
		ui.right_resolution->setVisible((backend == CAP_DSHOW)); */
#endif
	});
	connect(ui.right_properties, &QAbstractButton::clicked, this,
	[=]()
	{
		/* FIXME */
		/* cap_right.set(CAP_PROP_SETTINGS, 0); */
	});

	connect(ui.calibrate, &QAbstractButton::clicked, this,
	[=]()
	{
		/* close the camera streams if active */
		if (vdev_left)
			vdev_left->close();
		if (vdev_right)
			vdev_right->close();

		CalibWizard *cal = new CalibWizard(this);
		cal->exec();
		delete cal;

		load();

		/* restart the camera streams */
		if (vdev_left && vdev_left->open() == 0)
			vdev_left->stream(ui.left, true, false);
		if (vdev_right && vdev_right->open() == 0)
			vdev_right->stream(ui.right, true, false);
	});

	QSizePolicy warningsp = ui.save_warning->sizePolicy();
	warningsp.setRetainSizeWhenHidden(true);
	ui.save_warning->setSizePolicy(warningsp);
	ui.save_warning->setVisible(false);

	connect(ui.save_auto, &QAbstractButton::toggled, this,
	[=](bool t)
	{
		settings.setValue("save/auto", t);
	});
	connect(ui.save_path, &QLineEdit::textChanged, this,
	[=](QString s)
	{
		if (s.isEmpty() || QDir(s).exists()) {
			ui.save_warning->setVisible(false);
		} else {
			ui.save_warning->setVisible(true);
			s = "";
		}

		settings.setValue("save/path", s);
	});
	connect(ui.save_browse, &QAbstractButton::clicked, this,
	[=]()
	{
		/* close the camera streams if active */
		if (vdev_left)
			vdev_left->close();
		if (vdev_right)
			vdev_right->close();

		QFileDialog fd(this);
		fd.setFileMode(QFileDialog::Directory);
		if (fd.exec())
			ui.save_path->setText(fd.selectedFiles()[0]);

		/* restart the camera streams */
		if (vdev_left && vdev_left->open() == 0)
			vdev_left->stream(ui.left, true, false);
		if (vdev_right && vdev_right->open() == 0)
			vdev_right->stream(ui.right, true, false);
	});

	connect(ui.about, &QAbstractButton::clicked, this,
	[=]()
	{
		/* close the camera streams if active */
		if (vdev_left)
			vdev_left->close();
		if (vdev_right)
			vdev_right->close();

		About *about = new About(this);
		about->exec();
		delete about;

		/* restart the camera streams */
		if (vdev_left && vdev_left->open() == 0)
			vdev_left->stream(ui.left, true, false);
		if (vdev_right && vdev_right->open() == 0)
			vdev_right->stream(ui.right, true, false);
	});

	connect(ui.exit, &QAbstractButton::clicked, this, &QWidget::close);

	/* hide these buttons by default */
	ui.left_properties->setVisible(false);
	ui.right_properties->setVisible(false);

	/* load settings into labels */
	load();

	/* assemble device list */
	populate();
}

Settings::~Settings()
{
	if (vdev_left)
		vdev_left->close();
	if (vdev_right)
		vdev_right->close();
}

/* resize pixmaps on window resize */
void Settings::resizeEvent(QResizeEvent *e)
{
	(void) e;

	if (vdev_left)
		vdev_left->updateSurface(ui.left, true);
	if (vdev_right)
		vdev_right->updateSurface(ui.right, true);
}

/* populate the lists of cameras */
void Settings::populate()
{
	QStandardItemModel *model;

	model = dynamic_cast<QStandardItemModel *>(ui.left_select->model());
	model->item(0, 0)->setEnabled(false);

	model = dynamic_cast<QStandardItemModel*>(ui.right_select->model());
	model->item(0, 0)->setEnabled(false);

	ui.left_select->insertSeparator(1);
	ui.right_select->insertSeparator(1);

	for (auto &videodev : videodevices()) {
		ui.left_select->addItem(videodev->name);
		ui.right_select->addItem(videodev->name);
	}

	if (settings.contains("camera/left/index"))
		ui.left_select->setCurrentIndex(settings.value("camera/left/index").toInt() + 2);

	if (settings.contains("camera/right/index"))
		ui.right_select->setCurrentIndex(settings.value("camera/right/index").toInt() + 2);
}

/*
 * retrieve calibration settings from the settings file and write them to the
 * display
 */
void Settings::load()
{
	/*
	 * get all settings from this location:
	 * - Linux: ~/.config/babymeter/babymeter3.conf
	 * - macOS: [Application Home Directory]/Settings/babymeter/babymeter3
	 * - Windows: HKCU\Software\babymeter\babymeter3
	 */

	ui.left->setText("");
	ui.right->setText("");

	double rot, trans;

	settings.beginGroup("calibration");

	/* get the date of the last calibration */
	if (settings.contains("date")) {
		ui.date->setText(settings.value("date").toString());
		ui.calibrate->setText(tr("Re-calibrate"));
	} else {
		ui.date->setText(tr("never"));
	}

	/*
	 * calculate the translation between cameras by taking the Euclidean
	 * norm of the translation matrix
	 */
	if (settings.contains("translation")) {
		trans = norm(stringtomat(settings.value("translation").toString()));
		ui.translation->setText(QString::number(trans, 'f', 2) + " mm");
	} else {
		ui.translation->setText("-");
	}

	/* calculate the angle between cameras */
	if (settings.contains("rotation")) {
		rot = calcrotation(stringtomat(settings.value("rotation").toString()));
		ui.rotation->setText(QString::number(rot, 'f', 2) + "°");
	} else {
		ui.rotation->setText("-");
	}

	/* get the reprojection error */
	if (settings.contains("error"))
		ui.reprojectionerror->setText(QString::number(settings.value("error").toDouble(), 'f', 6) + tr(" px"));
	else
		ui.reprojectionerror->setText("-");

	/* get board properties */
	if (settings.contains("boardwidth") && settings.contains("boardheight"))
		ui.boardsize->setText(settings.value("boardwidth").toString() + " × "
				+ settings.value("boardheight").toString() + " squares");
	else
		ui.boardsize->setText("-");

	if (settings.contains("squaresize"))
		ui.squaresize->setText(settings.value("squaresize").toString() + " mm");
	else
		ui.squaresize->setText("-");

	settings.endGroup();

	settings.beginGroup("save");

	if (settings.contains("auto"))
		ui.save_auto->setChecked(settings.value("auto").toBool());
	if (settings.contains("path"))
		ui.save_path->setText(settings.value("path").toString());

	settings.endGroup();
}
