/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#ifdef DAHENG

#include <videodevice.h>

#include <GxIAPI.h>

class DahengAcquisitionThread : public AcquisitionThread
{
	Q_OBJECT

private:
	GX_DEV_HANDLE handle;
	int64_t payloadsize, width, height, colorfilter;
	char *rawframe, *rgbframe;

	static void __stdcall callback(GX_FRAME_CALLBACK_PARAM *params);

public:
	explicit DahengAcquisitionThread(QObject *parent = 0) {};
	~DahengAcquisitionThread();

	void setDeviceHandle(GX_DEV_HANDLE dev);

protected:
	void run();
};

class DahengVideoDevice : VideoDevice
{
	Q_OBJECT

private:
	GX_DEV_HANDLE handle;
	char serial[GX_INFO_LENGTH_32_BYTE];
	bool opened {false};

public:
	DahengVideoDevice(const char *friendlyname, char *sn);

	int open();
	void close();

	int startAcq();
	void stopAcq();

	bool isOpen();
	void loadFrame();
};

class DahengVideoDeviceManager : public VideoDeviceManager
{
private:
	QList<VideoDevice *> devices;

public:
	DahengVideoDeviceManager();
	~DahengVideoDeviceManager();

	QList<VideoDevice *> enumerate();
};

#endif
