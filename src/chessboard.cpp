/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <chessboard.h>

#include <QRandomGenerator>
#include <QtMath>

Chessboard::Chessboard(QWidget *parent) : QWidget(parent) {
	ui.setupUi(this);
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

	ui.viewport->setScene(&scene);
}

void Chessboard::initialize(int width, int height, float mm) {
	// Set chessboard dimensions
	chessboardWidth = width + 1;
	chessboardHeight = height + 1;

	// Millimeter -> Inch -> Pixels
	squareSize = (mm / 25.4f) * screen()->physicalDotsPerInch();

	// Create checkerboard graphics rect objects
	QList<QGraphicsItem *> list;
	for (int y = 0; y < chessboardHeight; y++) {
		for (int x = 0; x < chessboardWidth; x++) {
			list.append(scene.addRect(
				QRectF(
					x * squareSize - (chessboardWidth * squareSize / 2),
					y * squareSize - (chessboardHeight * squareSize / 2),
					squareSize,
					squareSize
				),
				Qt::NoPen,
				x % 2 == y % 2 ? QBrush(Qt::white) : QBrush(Qt::black)
			));
		}
	}
	board = scene.createItemGroup(list);

	// Move chessboard to first random position
	nextPos();
}

/* move the chessboard to a different random position and orientation */
void Chessboard::nextPos() {
	if (board == nullptr) {
		return;
	}

	// Get screen width and height
	int screenWidth = screen()->size().rwidth();
	int screenHeight = screen()->size().rheight();

	// Calculate screen padding pixels
	int padding = squareSize / 4;

	// Calculate horizontal space
	int horizontalSpace = screenWidth - (padding * 2) - (chessboardWidth * squareSize);

	// Calculate vertical space
	int verticalSpace = screenHeight - (padding * 2) - (chessboardHeight * squareSize);

	// Calculate random position and angle
	int x = QRandomGenerator::global()->bounded(0, horizontalSpace) + (chessboardWidth * squareSize / 2) + padding;
	int y = QRandomGenerator::global()->bounded(0, verticalSpace) + (chessboardHeight * squareSize / 2) + padding;
	int angle = QRandomGenerator::global()->bounded(0, 180);

	// Check when rotate if clipping outside screen
	if (angle != 0 && angle != 180) {
		int halfSize = sqrt(
			pow(chessboardWidth * squareSize / 2, 2) +
			pow(chessboardHeight * squareSize / 2, 2)
		);

		// If so push back to padding border
		if (x - halfSize < padding) x = halfSize + padding;
		if (y - halfSize < padding) y = halfSize + padding;
		if (x + halfSize > screenWidth - padding) x = screenWidth - halfSize - padding;
		if (y + halfSize > screenHeight - padding) y = screenHeight - halfSize - padding;
	}

	// Set transform for the chessboard
	QTransform transform;
	transform.translate(x, y);
	transform.rotate(angle);
	board->setTransform(transform);
}

int Chessboard::getSquareSizeInPixels(QScreen *screen) {
	// Get screen width and height
	int screenWidth = screen->size().rwidth();
	int screenHeight = screen->size().rheight();

	// Checkboard is 50% of screen
	if (screenWidth < screenHeight) {
		return ((float)screenWidth * 0.5f) / CHESSBOARD_WIDTH;
	} else {
		return ((float)screenHeight * 0.5f) / CHESSBOARD_HEIGHT;
	}
}

float Chessboard::getSquareSize(QScreen *screen) {
	// Pixels -> Inch -> Millimeter
	return ((float) getSquareSizeInPixels(screen) / screen->physicalDotsPerInch()) * 25.4f;
}
