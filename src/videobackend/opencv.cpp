/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <videobackend/opencv.h>

OpenCVVideoDevice::OpenCVVideoDevice(int n)
{
	name = tr("Device ") + QString::number(n);
	index = n;
}

OpenCVVideoDevice::~OpenCVVideoDevice()
{
	close();
}

int OpenCVVideoDevice::open()
{
	/* open the device if it wasn't already */
	if (isOpen())
		return 0;

	videocap.open(index);

	if (!isOpen())
		return -1;

	return 0;
}

void OpenCVVideoDevice::close()
{
	if (!isOpen())
		return;

	/* stop acquisition and drawing to surfaces first */
	stop();

	videocap.release();
}

int OpenCVVideoDevice::startAcq()
{
	if (acqthread)
		return 0;

	/* start a separate thread to prepare for acquisition */
	if (!(acqthread = new OpenCVAcquisitionThread(this)))
		return -1;
	((OpenCVAcquisitionThread *) acqthread)->setVideoCapture(videocap);
	acqthread->start();

	return 1;
}

void OpenCVVideoDevice::stopAcq()
{
	if (!acqthread)
		return;

	acqthread->surfacecount = 0;
	acqthread->exit();
	acqthread->wait();

	delete acqthread;
	acqthread = nullptr;

	return;
}

bool OpenCVVideoDevice::isOpen()
{
	return videocap.isOpened();
}

void OpenCVVideoDevice::loadFrame()
{
	if (isOpen())
		image = acqthread->getFrame();
}

OpenCVVideoDeviceManager::OpenCVVideoDeviceManager()
{
	cv::VideoCapture videocap;

	for (int i = 0;; i++) {
		videocap.open(i);

		if (!videocap.isOpened())
			break;

		devices.append((VideoDevice *) new OpenCVVideoDevice(i));

		videocap.release();
	}
}

QList<VideoDevice *> OpenCVVideoDeviceManager::enumerate()
{
	return devices;
}
