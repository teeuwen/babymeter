/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "ui_main.h"

#include <videodevice.h>

#include <QSettings>
#include <QStandardItemModel>
#include <QStandardItem>

#define UI_FONT		"Open Sans"
#define UI_COLOR	"#34323D"

class Main : public QWidget
{
	Q_OBJECT

public:
	explicit Main(QWidget *parent = nullptr);

private slots:
	void clearpoints();
	bool undolastpoint();

	void live();
	void capture();

	void calculate(bool save);

private:
	/* Controls Pages */
	enum {
		CAPTURE,
		SELECT,
		REVIEW
	};

	Ui::Main ui;
	QSettings settings;

	const QColor NORMAL_COLOR = Qt::white;
	const QColor PRESSED_COLOR = QColor("#B3B3B3");

	void button_press(QToolButton *btn);
	void button_release(QToolButton *btn);

	/* video stream input devices */
	VideoDevice *vdev_left = nullptr, *vdev_right = nullptr;

	/*
	 * timestamp of the last capture, used as directory name if saving is
	 * enabled
	 */
	QString lastcapture;

	/* saving */
	void load_session(QString timestamp);
	void save_capture();
	void save_points(QVector<QPointF> &left, QVector<QPointF> &right);
	void save_results(QVector<float> &segments, std::vector<cv::Point2f> &left, std::vector<cv::Point2f> &right);

	void closeDevices();

protected:
	virtual void resizeEvent(QResizeEvent *e) override;
};
