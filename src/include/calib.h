/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "ui_calibsetup.h"
#include "ui_calibcapture.h"
#include "ui_calibreview.h"

#include <chessboard.h>
#include <videodevice.h>

#include <QCloseEvent>
#include <QFutureWatcher>
#include <QSettings>
#include <QShowEvent>
#include <QTimer>
#include <QtConcurrent/QtConcurrentRun>

/* multiplier for autocapture slider and timer in ms */
#define AUTOCAP_MULTIPLIER	500

/*
 * time to wait between capturing during automatic calibration in units of
 * AUTOCAP_MULTIPLIER
 */
#define AUTOCALIB_TIMEOUT	1

/* timeout after capturing a sample in ms */
#define CAPTURE_TIMEOUT		1500

/* minimum amount of samples required for calibration */
#define MINSAMPLES		5

class CalibWizard : public QWizard
{
	Q_OBJECT

private:
	int close_confirm();

public:
	/* Wizard Pages */
	enum {
		Setup,
		Capture,
		Review
	};

	CalibWizard(QWidget *parent = nullptr);

	virtual void reject() override;

protected:
	virtual void closeEvent(QCloseEvent *e) override;
};

/*
 * Setup Page
 */
class CalibSetup : public QWizardPage
{
	Q_OBJECT

private:
	Ui::CalibSetup ui;
	QSettings settings;

	QFuture<void> imgloading;
	QFutureWatcher<void> watchimgloading;
	bool cancelimgloading = false, imgsloaded = false;
	void loadimgs(const QString &lpath, const QString &rpath);

public:
	explicit CalibSetup(QWidget *parent = nullptr);

	virtual bool validatePage() override;
	virtual bool isComplete() const override;
	virtual int nextId() const override;

private slots:
	void updateinterval(int num);

signals:
	void file_load(int i, int max);
};

/*
 * Capture Page
 */
class CalibCapture : public QWizardPage
{
	Q_OBJECT

private:
	Ui::CalibCapture ui;
	Chessboard *chessboard;
	QSettings settings;

	QFuture<bool> finding;
	QFutureWatcher<bool> watchfinding;
	bool findCorners(const int boardwidth, const int boardheight);

	/* video stream input devices */
	VideoDevice *vdev_left = nullptr, *vdev_right = nullptr;

	/* timers */
	QTimer *capturetimer;
	QTimer *autocaptimer;
	unsigned int autocaptimeout;

public:
	explicit CalibCapture(QWidget *parent = nullptr);

	virtual void initializePage() override;
	virtual void cleanupPage() override;
	virtual bool validatePage() override;
	virtual bool isComplete() const override;

private slots:
	void live();
	void capture();
	void autocapture();

protected:
	virtual void resizeEvent(QResizeEvent *e) override;
};

/*
 * Review Page
 */
class CalibReview : public QWizardPage
{
	Q_OBJECT

private:
	Ui::CalibReview ui;
	QSettings settings;

	QFuture<void> calibrating;
	QFutureWatcher<void> watchcalibrating;
	void calibrate(const int boardwidth, const int boardheight, const float squaresize);

	/* current image */
	unsigned int index;

	void cycle(int dir);
	void discard();

public:
	explicit CalibReview(QWidget *parent = nullptr);

	virtual void initializePage() override;
	virtual void cleanupPage() override;
	virtual bool validatePage() override;
	virtual bool isComplete() const override;

protected:
	virtual void showEvent(QShowEvent *ev) override;
	virtual void resizeEvent(QResizeEvent *e) override;

signals:
	void calibrate_status(QString str);
};
