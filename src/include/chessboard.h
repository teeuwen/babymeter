/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "ui_chessboard.h"

#include <QGraphicsItemGroup>
#include <QScreen>

class Chessboard : public QWidget
{
	Q_OBJECT

private:
	Ui::Chessboard ui;
	QGraphicsScene scene;
	QGraphicsItemGroup *board = nullptr;
	int chessboardWidth = CHESSBOARD_WIDTH, chessboardHeight = CHESSBOARD_HEIGHT;
	int squareSize;

	static int getSquareSizeInPixels(QScreen *screen);

public:
	static const int CHESSBOARD_WIDTH =	10;
	static const int CHESSBOARD_HEIGHT =	7;

	explicit Chessboard(QWidget *parent = nullptr);

	void initialize(int width, int height, float mm);
	void nextPos();

	static float getSquareSize(QScreen *screen);
};
