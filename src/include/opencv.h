/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <QPixmap>
#include <QSettings>
#include <QString>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>

QPixmap mattopixmap(cv::Mat const &src);

QString mattostring(cv::Mat src);
cv::Mat stringtomat(QString _src);

bool opencam(cv::VideoCapture &cap, QSettings &settings, const QString key);

int corners_find(cv::Mat &img, std::vector<cv::Point2f> &points, int w, int h);
void corners_draw(cv::Mat &img, std::vector<cv::Point2f> &points, int w, int h);

std::vector<cv::Point3f> getobj(int w, int h, float n);

double calcrotation(cv::Mat src);
