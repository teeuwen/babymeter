# General
Babymeter is an open-source application to measure the body (parts) length of
premature babies in for example an incubator. This application was developed on
behalf of _Create4Care_. Create4Care is a division of _Erasmus MC, Rotterdam_
that focusses on researching and designing various utilities for the medical
word utilizing innovative and smart technologies.

This particular project is built in collaboration with students from the studies
Computer Engineering (Technische Informatica) Industrial Design (Industrieel
Product Ontwerpen) from Hogeschool Rotterdam.

![Screenshot](doc/screenshot.png)


# Development
This application is just a research project for now and is not tested for use in
a production environment.

Our Scrum/issues board can be found on [Trello](https://trello.com/b/TvzwNz9R/scrum-board).


# Features
Babymeter includes the following features:

- A cross-platform, touch-screen friendly, interface written using the _Qt
  Toolkit_
- Stereoscopic calibration using samples from live cameras or from a folder
- Support for _GenICam_ cameras on Linux and macOS through _Aravis_
- Support for _Daheng_ (and perhaps other) cameras on Windows through DirectShow
- Measurement in real-word units of vertices in a 3D point cloud generated using
  stereoscopy using _OpenCV_
- Importing and exporting samples to an external file
- Exporting samples to JPEG and exporting results to CSV

## Keyboard shortcuts
- `F11` to toggle full screen
- `Ctrl/⌘  + Z` to undo setting your last point
- `Del` or `Ctrl + D` to clear all points

## Touchscreen support
Running babymeter on touch screens is fully supported.

On Linux platforms you need to let the application to know that touch input is
preferred. Therefore the following environment variable must be set beforehand:
```
QT_IM_MODULE=qtvirtualkeyboard
```


# Building
To build with debugging support (compile with debugging symbols, include verbose
output on the console, skip the splash screen, among other tiny tweaks), build
with `-DCMAKE_BUILD_TYPE=Debug`.

In case you're compiling for use in a production environment, build with
`-DCMAKE_BUILD_TYPE=Release`.

## Linux
1. Install a build environment, git, Qt5 and OpenCV. This process differs per
   distribution, the process for Arch Linux and Debian/Ubuntu is described
   below.
2. Clone this repository and create a build directory:
```
$ git clone https://gitlab.com/teeuwen/babymeter.git
$ mkdir babymeter/src/build && cd babymeter/src/build
```
3. Build `babymeter`:
```console
$ cmake ../. && make -j$(nproc)
```
4. Install `babymeter` and run (make sure /usr/local/bin is in your `PATH`):
```console
$ make install
$ babymeter
```

### Arch Linux
```console
# pacman -S base-devel git git-lfs cmake qt5-base qt5-svg vtk hdf5 glew opencv
```
### Ubuntu
```console
# apt install build-essential git git-lfs cmake qt5-default libopencv-dev libqt5svg5-dev
```
### Other distributions
Other distros are possibly also supported. The Linux distributions and operating
systems covered in this README are the only ones tested.


## macOS
Only building with Homebrew on macOS is officially supported and tested.

1. Install Homebrew:
```console
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```
2. Install XCode Command Line Tools, which contains the macOS build toolchain:
```console
$ xcode-select --install
```
3. Install git, Qt5 and OpenCV:
```console
$ brew install git git-lfs cmake qt5 opencv
```
4. Clone this repository and enter the `src/` directory:
```
$ git clone https://gitlab.com/teeuwen/babymeter.git
$ mkdir babymeter/src/build && cd babymeter/src/build
```
5. Build `babymeter`:
```console
$ cmake ../. && make -j$(nproc)
```

### GenICam support
See the GenICam support section for Linux.

## Windows
Pre-built binaries are also available (see "Releases" in the left column on
GitLab). The binaries can be decompressed using 7-Zip or from the command line
using `unlzma [name].exe.lzma`.

By default, it is only supported to build statically with the provided OpenCV
4.5.1 binaries as MSYS2 doesn't provide statically pre-built OpenCV binaries.
The pre-built OpenCV binaries I've included only contain the minimum required
build options to run babymeter.

Only building with MinGW on x86\_64 Windows is officially supported and tested.
Building with Visual Studio should also be possible in theory.

1. Install x86\_64 [MSYS2](https://www.msys2.org/) from their official website
2. Update the MSYS2:
```console
$ pacman -Syu
```
3. Install the MinGW x86\_64 toolchain, git, statically-built Qt5, and Zstandard:
```console
$ pacman -S mingw-w64-x86_64-toolchain git mingw-w64-x86_64-git-lfs mingw-w64-x86_64-cmake mingw-w64-x86_64-qt5-static mingw-w64-x86_64-zstd
```
4. Add Qt5's `bin/` folder to your system's PATH env variable:
```console
$ echo 'export PATH="/mingw64/qt5-static/bin:$PATH"' > /etc/profile.d/qt5-static.sh
$ source /etc/profile.d/qt5-static.sh
```
5. Clone this repository and enter the `src/` directory:
```
$ git clone https://gitlab.com/teeuwen/babymeter.git
$ mkdir babymeter/src/build && cd babymeter/src/build
```
6. Build `babymeter.exe`:
```console
$ export CMAKE_PREFIX_PATH=/mingw64/qt5-static/lib/cmake/
$ cmake -G "MinGW Makefiles" ../. && mingw32-make -j$(nproc)
```

## Daheng cameras
Daheng Imaging machine vision camera support is available on Windows (x86\_64)
and Linux (x84\_64, armv7 and armv8) using its native GxIAPI.

NOTE: Linux is supported by GxIAPI, but hasn't been properly integrated into the
build system yet. We're working on this.

Compile with `-DWITH_DAHENG=ON` for support on the respective platforms. On
Windows, be sure to also install the
[driver](https://www.get-cameras.com/customerdownloads).

# Credits
**Babymeter 3**

Happy Measure, Stereoscopic Vision Body Size Measuring Instrument\
by Create4Care - Erasmus MC 2020

**Developed by**\
Bastiaan Teeuwen \<bastiaan@mkcl.nl\>\
David Kakes \<davidkakes@gmail.com\>

**In cooperation with**\
Bram van Woerkens\
Jordy Weijgertse\
Meike Campen\
Merel Venema\
Thomas van der Helm\
Yueming Tan

**On behalf of and thanks to**\
Ronald van Gils\
Timothy Singowikromo


# Licensing
The license for this application is contained in the `LICENSE` file in the root
directory. Licenses for the pre-built OpenCV binaries and it's dependencies can
be found in `src/opencv/` and `src/opencv/licenses`.

All other licenses of dependencies used to build the program can also be found
at runtime when going to `Settings` and clicking `About` in the bottom-left
corner.
