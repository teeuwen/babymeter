/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 David Kakes <davidkakes@gmail.com>
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <imageview.h>

#include <main.h>
#include <math.h>

Dot::Dot(qreal x, qreal y, qreal width, qreal height, QGraphicsItem *parent) :
		QGraphicsEllipseItem(x, y, width, height, parent) { }

/* prevent drawing selection box on dot */
void Dot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	QStyleOptionGraphicsItem opt(*option);
	opt.state &= ~QStyle::State_Selected;

	QGraphicsEllipseItem::paint(painter, &opt, widget);
}

ImageView::ImageView(QWidget *parent) : QGraphicsView(parent)
{
	setScene(&scene_main);

	magnif = new QGraphicsView(this->parentWidget());
	magnif->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	magnif->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	magnif->setFrameStyle(QFrame::NoFrame);

	magnif->setScene(&scene_magnif);
	magnif->setVisible(false);

	scene_magnif.setBackgroundBrush(QBrush(QColor(UI_COLOR), Qt::SolidPattern));
}

void ImageView::setPixmap(const QPixmap &pixmap)
{
	if (!pix_main || !pix_magnif) {
		pix_main = scene_main.addPixmap(pixmap);
		pix_magnif = scene_magnif.addPixmap(pixmap);
		magnif->scale(MAGNIF_ZOOM, MAGNIF_ZOOM);

		/* draw the crosshair in the magnifying glass scene */
		QPen pen(Qt::red);
		pen.setWidth(1);

		vline = scene_magnif.addLine(0, 0, 0, magnif->height());
		vline->setPen(pen);

		hline = scene_magnif.addLine(0, 0, magnif->width(), 0);
		hline->setPen(pen);
	} else {
		pix_main->setPixmap(pixmap);
		pix_magnif->setPixmap(pixmap);
	}

	scene_main.setSceneRect(0, 0, pixmap.width(), pixmap.height());
	fitInView(scene_main.sceneRect(), Qt::KeepAspectRatio);
}

/* FIXME take pixmap coordinates */
void ImageView::setCoordinates(QVector<QPointF> &pnts)
{
	const double radius = DOT_RADIUS / 100 * scene()->width();
	clearPoints();

	for (auto &pnt : pnts)
		addPoint(pnt, radius);

	update();
}

/* FIXME return pixmap coordinates */
QVector<QPointF> ImageView::getCoordinates()
{
	QVector<QPointF> points;

	for (auto &dot : dots)
		points.append(getPoint(dot));

	return points;
}

void ImageView::setSelEnabled(bool enable)
{
	selenabled = enable;

	if (enable)
		viewport()->setCursor(Qt::CrossCursor);
	else
		viewport()->unsetCursor();
}

void ImageView::clearPoints()
{
	if (dots.empty())
		return;

	for (auto &dot : dots)
		scene()->removeItem(getHitbox(dot));
	dots.clear();

	for (auto &line : lines)
		scene()->removeItem(line);
	lines.clear();

	update();
}

void ImageView::undoLastPoint()
{
	if (dots.empty())
		return;

	scene()->removeItem(getHitbox(dots.last()));

	if (dots.size() > 1) {
		scene()->removeItem(lines.last());
		lines.removeLast();
	}
	dots.removeLast();

	update();
}

/* return the currently selected Dot */
inline Dot *ImageView::getSelectedDot()
{
	if (!scene()->selectedItems().size() || !scene()->selectedItems()[0]->childItems().size())
		return nullptr;

	return qgraphicsitem_cast<Dot *>(scene()->selectedItems()[0]->childItems()[0]);
}

inline Dot *ImageView::getHitbox(Dot *dot)
{
	if (!dot || !dot->parentItem())
		return nullptr;

	return qgraphicsitem_cast<Dot *>(dot->parentItem());
}

void ImageView::addPoint(QPointF &point, const double radius)
{

	const double text_placement =  TEXT_PLACEMENT / 100 * scene()->width();
	const double text_size = TEXT_SIZE / 100 * scene()->width();
	const double line_width = LINE_WIDTH / 100 * scene()->width();

	/* invisible hitbox item to increase selection box */
	Dot *hitbox = new Dot(0, 0, (HITBOX_RADIUS + radius) * 2, (HITBOX_RADIUS + radius) * 2);
	hitbox->setPos(point.x() - radius - HITBOX_RADIUS, point.y() - radius - HITBOX_RADIUS);
	hitbox->setZValue(1);
	hitbox->setFlag(QGraphicsItem::ItemIsSelectable, true);
	hitbox->setFlag(QGraphicsItem::ItemIsMovable, true);
	hitbox->setBrush(Qt::NoBrush);
	hitbox->setPen(Qt::NoPen);
	hitbox->setCursor(Qt::OpenHandCursor);
	scene()->addItem(hitbox);

	/* draw the dot */
	Dot *dot = new Dot(0, 0, radius * 2, radius * 2, hitbox);
	dot->setPos(HITBOX_RADIUS, HITBOX_RADIUS);
	dot->setZValue(2);
	dot->setBrush(QBrush(Qt::red));
	dots.append(dot);

	/*
	 * FIXME
	 * adjust the position of the text label if it's out of scene bounds or
	 * overlapping another item like a line or dot
	 */
	/* set text to the dots' index */
	QGraphicsTextItem *text = new QGraphicsTextItem(dot);
	text->setPos(radius, - text_placement);
	text->setZValue(3);
	text->setFont(QFont(UI_FONT, text_size, 1));
	text->setDefaultTextColor(Qt::blue);
	text->setPlainText(QString::number(dots.size()));

	/* draw the line */
	QPen pen(Qt::yellow);
	pen.setWidth(line_width);

	if (dots.size() > 1) {
		QGraphicsLineItem *line = scene()->addLine(calcLine(dot, dots[dots.size() - 2]), pen);

		lines.append(line);
	}

	update();
}

/* return the center of a QGraphicsItem */
inline QPointF ImageView::getPoint(QGraphicsItem *dot)
{
	return dot->sceneBoundingRect().center();
}

/* return the QGraphicsTextItem child of a QGraphicsItem */
QGraphicsTextItem *ImageView::getText(QGraphicsItem *dot)
{
	for (auto &item : dot->childItems()) {
		QGraphicsTextItem *text = qgraphicsitem_cast<QGraphicsTextItem *>(item);

		if (text)
			return text;
	}

	return nullptr;
}

/* move the magnifiying glass to the currently selected point */
void ImageView::moveMagnif(Dot *dot)
{
	const double radius = mapFromScene(sceneRect()).boundingRect().width() * MAGNIF_SCALE / 100;

	/* calculate new radius */
	magnif->setFixedSize(radius * 2, radius * 2);

	/* calculate new mask size */
	const int side = qMin(magnif->width(), magnif->height());

	QRegion mask(magnif->width() / 2 - side / 2, magnif->height() / 2 - side / 2, side, side, QRegion::Ellipse);
	magnif->setMask(mask);

	/* resize and move the crosshair in the scene */
	QPointF point = getPoint(dot);
	const double sceneradius = magnif->width();

	vline->setLine(0, 0, 0, sceneradius);
	vline->setPos(point.x(), point.y() - sceneradius / 2);

	hline->setLine(0, 0, sceneradius, 0);
	hline->setPos(point.x() - sceneradius / 2, point.y());

	magnif->centerOn(point);

	/* move the magnifying glass */
	const int magnif_placement = MAGNIF_PLACEMENT / 100 * height();

	QPointF pos = mapTo(window(), mapFromScene(point));
	magnif->move(pos.x() - magnif->width() / 2, pos.y() - magnif_placement - magnif->height());
}

/* prevent collision between two Dots by adjusting their position */
void ImageView::preventCollision(Dot *dot1, Dot *dot2)
{
	if (!dot1->collidesWithItem(dot2))
		return;

	QPointF pos, a, b;

	a = dot2->scenePos();
	b = dot1->scenePos();

	/* https://math.stackexchange.com/questions/127613/closest-point-on-circle-edge-from-point-outside-inside-the-circle */
	pos.rx() = a.x() + dot1->boundingRect().width() * (b.x() - a.x()) /
			sqrt(pow(b.x() - a.x(), 2) + pow(b.y() - a.y(), 2));
	pos.ry() = a.y() + dot1->boundingRect().height() * (b.y() - a.y()) /
			sqrt(pow(b.x() - a.x(), 2) + pow(b.y() - a.y(), 2));

	/* FIXME items are allowed to collide on the edges of the scene */
	/* adjust the position of the hitbox */
	if (pos.x() > 0 && pos.y() > 0)
		getHitbox((Dot *) dot1)->setPos(pos.x() - HITBOX_RADIUS, pos.y() - HITBOX_RADIUS);
}

/* create new line between the center of two QGraphicsItems */
QLineF ImageView::calcLine(QGraphicsItem *item1, QGraphicsItem *item2)
{
	QPointF off1, off2;
	QPointF p1, p2;
	QLineF line;

	off1 = getPoint(item1);
	off2 = getPoint(item2);

	double phi = atan2(off1.y() - off2.y(), off1.x()- off2.x());

	p1.setX(off1.x() - item1->boundingRect().width() / 2 * cos(phi));
	p1.setY(off1.y() - item1->boundingRect().height() / 2 * sin(phi));
	p2.setX(off2.x() + item1->boundingRect().width() / 2 * cos(phi));
	p2.setY(off2.y() + item1->boundingRect().height() / 2 * sin(phi));
	line.setPoints(p1, p2);

	return line;
}

void ImageView::mousePressEvent(QMouseEvent *ev)
{
	QGraphicsView::mousePressEvent(ev);

	if (ev->button() != Qt::LeftButton || !selenabled)
		return;

	Dot *dot;

	/* check if already existant item is being pressed */
	if ((dot = getSelectedDot())) {
		/* show the grab cursor and show the magnifying glass */
		getHitbox(dot)->setCursor(Qt::ClosedHandCursor);

		magnif->setVisible(true);
		magnif->raise();
		moveMagnif(dot);

		return;
	}

	QPointF point = mapToScene(ev->pos());
	const double radius = DOT_RADIUS / 100 * scene()->width();

	/* check if mousepress is within scene bounds */
	if (point.x() - radius < 0 || point.y() - radius < 0)
		return;
	if (point.x() + radius >= scene()->width() || point.y() + radius >= scene()->height())
		return;

	addPoint(point, radius);

	emit mousePressed();
}

void ImageView::mouseReleaseEvent(QMouseEvent *ev)
{
	QGraphicsView::mouseReleaseEvent(ev);

	if (ev->button() != Qt::LeftButton || !selenabled || !getSelectedDot())
		return;

	/* set the ungrab cursor and hide the magnifying glass */
	getHitbox(getSelectedDot())->setCursor(Qt::OpenHandCursor);
	magnif->setVisible(false);

	emit mousePressed();
}

/*
 * for some reason, double clicking doesn't call mouseMoveEvent, so we disabled
 * it
 */
void ImageView::mouseDoubleClickEvent(QMouseEvent *ev)
{
	(void) ev;

	if (ev->button() != Qt::LeftButton || !selenabled || !getSelectedDot())
		return;

	const int index = dots.indexOf(getSelectedDot());

	/* deal with the tragedy of removing lines */
	if (dots[index] != dots.first()) {
		scene()->removeItem(lines[index - 1]);
		lines.removeAt(index - 1);
	}
	if (dots[index] != dots.last()) {
		if (dots[index] != dots.first() && dots.size() > 2) {
			lines[index - 1]->setLine(calcLine(dots[index - 1], dots[index + 1]));
		} else {
			scene()->removeItem(lines[index]);
			lines.removeAt(index);
		}
	}

	scene()->removeItem(getHitbox(dots[index]));
	dots.removeAt(index);

	/* recalculate the text labels */
	for (int i = 0; i < dots.size(); i++)
		getText(dots[i])->setPlainText(QString::number(i + 1));

	emit mousePressed();
}

void ImageView::mouseMoveEvent(QMouseEvent *ev)
{
	QGraphicsView::mouseMoveEvent(ev);

	if (!(ev->buttons() & Qt::LeftButton) || !selenabled || !getSelectedDot())
		return;

	Dot *dot = getSelectedDot();
	const QRectF rect = dot->sceneBoundingRect();
	QPointF pos = dot->scenePos();

	/* check if item is moved out of scene and adjust pos if needed */
	if (rect.top() < 0)
		pos.ry() -= rect.top();
	if (rect.bottom() > scene()->height())
		pos.ry() = scene()->height() - dot->boundingRect().height();
	if (rect.left() < 0)
		pos.rx() -= rect.left();
	if (rect.right() > scene()->width())
		pos.rx() = scene()->width() - dot->boundingRect().width();

	/* adjust the position of the hitbox */
	getHitbox(dot)->setPos(pos.x() - HITBOX_RADIUS, pos.y() - HITBOX_RADIUS);

	for (auto &i : dots)
		if (i != dot)
			preventCollision(dot, i);

	/* recalculate the line */
	const int index = dots.indexOf(dot);
	if (index > 0)
		lines[index - 1]->setLine(calcLine(dots[index], dots[index - 1]));
	if (index < dots.size() - 1)
		lines[index]->setLine(calcLine(dots[index], dots[index + 1]));

	moveMagnif(dot);
}
