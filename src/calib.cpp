/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <calib.h>
#include <opencv.h>

#include <QApplication>
#include <QDateTime>
#include <QFileDialog>
#include <QtGlobal>
#include <QMessageBox>
#include <QWindow>

using namespace cv;

bool calibrated;

/* images buffer */
static std::vector<Mat> imgs_left, imgs_right;
/* points buffer */
static std::vector<std::vector<Point2f>> pnts_left, pnts_right;

CalibWizard::CalibWizard(QWidget *parent) : QWizard(parent)
{
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	setPage(Setup, new CalibSetup);
	setPage(Capture, new CalibCapture);
	setPage(Review, new CalibReview);

	setWindowTitle(tr("Calibration - Babymeter 3"));
	setMinimumSize(640, 522);
	setWizardStyle(QWizard::NStyles);
	setOptions(QWizard::NoBackButtonOnStartPage | QWizard::NoBackButtonOnLastPage);
	setWindowModality(Qt::WindowModal);

	/* clear points from previous calibration during same session */
	imgs_left.clear();
	pnts_left.clear();
	imgs_right.clear();
	pnts_right.clear();
	calibrated = false;
}

/* ask the user to discard all changes and exit calibration */
int CalibWizard::close_confirm()
{
	QMessageBox confirmclose(this);

	confirmclose.setText(tr("Are you sure you want to exit calibration?"));
	confirmclose.setInformativeText(tr("All progress will be discarded."));
	confirmclose.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	confirmclose.setDefaultButton(QMessageBox::No);
	confirmclose.setWindowModality(Qt::WindowModal);
	confirmclose.setWindowFlags(confirmclose.windowFlags() & ~Qt::WindowContextHelpButtonHint);

	return confirmclose.exec();
}

void CalibWizard::closeEvent(QCloseEvent *e)
{
	if (calibrated || close_confirm() == QMessageBox::Yes)
		e->accept();
	else
		e->ignore();
}

void CalibWizard::reject()
{
	if (close_confirm() == QMessageBox::Yes)
		QWizard::reject();
}

/*
 * Setup Page
 */
CalibSetup::CalibSetup(QWidget *parent) : QWizardPage(parent)
{
	ui.setupUi(this);

	connect(ui.source_virtual, &QAbstractButton::toggled, this,
	[=](bool checked) {
		if (!checked)
			return;

		ui.boardwidth->setValue(Chessboard::CHESSBOARD_WIDTH - 1);
		ui.boardheight->setValue(Chessboard::CHESSBOARD_HEIGHT - 1);
		ui.squaresize->setValue(Chessboard::getSquareSize(screen()));
	});

	connect(ui.source_file, &QAbstractButton::toggled, this, &CalibSetup::completeChanged);

	connect(ui.autocap_interval, &QAbstractSlider::valueChanged, this, &CalibSetup::updateinterval);

	connect(ui.left_path, &QLineEdit::textChanged, this, &CalibSetup::completeChanged);
	connect(ui.left_browse, &QAbstractButton::clicked, this,
	[=]() {
		QFileDialog fd(this);
		fd.setWindowTitle(tr("Open left samples directory"));
		fd.setFileMode(QFileDialog::Directory);

		if (fd.exec())
			ui.left_path->setText(fd.selectedFiles()[0]);
	});

	connect(ui.right_path, &QLineEdit::textChanged, this, &CalibSetup::completeChanged);
	connect(ui.right_browse, &QAbstractButton::clicked, this,
	[=]() {
		QFileDialog fd(this);
		fd.setWindowTitle(tr("Open right samples directory"));
		fd.setFileMode(QFileDialog::Directory);

		if (fd.exec())
			ui.right_path->setText(fd.selectedFiles()[0]);
	});

	connect(&watchimgloading, &QFutureWatcher<void>::finished, this,
	[=]() {
		/* restore controls */
		ui.dimencontrols->setEnabled(true);
		ui.squaresizecontrols->setEnabled(true);
		ui.source_physical->setEnabled(true);
		ui.source_virtual->setEnabled(true);
		ui.source_file->setEnabled(true);
		ui.left->setEnabled(true);
		ui.right->setEnabled(true);
		ui.cancel->setEnabled(true);
		ui.status->setVisible(false);

		repaint();

		if (!cancelimgloading) {
			imgsloaded = true;
			wizard()->next();
		} else {
			cancelimgloading = false;
		}

		emit completeChanged();
	});

	/* file loading status text */
	ui.status->setVisible(false);
	connect(this, &CalibSetup::file_load, this,
	[=](int i, int max) {
		ui.status_text->setText(tr("Loading sample ") +
				QString::number(i) + "/" + QString::number(max)
				+ tr("..."));
	});

	connect(ui.cancel, &QAbstractButton::clicked, this,
	[=]() {
		ui.cancel->setEnabled(false);
		repaint();

		cancelimgloading = true;
	});

	/*
	 * show option to display chessboard on a second monitor only if one is
	 * attached
	 */
	if (((QGuiApplication *) QCoreApplication::instance())->screens().size() < 2)
		ui.secondmon->setVisible(false);

	registerField("boardwidth", ui.boardwidth);
	registerField("boardheight", ui.boardheight);
	registerField("squaresize", ui.squaresize, "value", SIGNAL(valueChanged));
	registerField("source_virtual", ui.source_virtual);
	registerField("source_file", ui.source_file);
	registerField("autocap", ui.autocap);
	registerField("autocap_interval", ui.autocap_interval);
	registerField("secondmon", ui.secondmon);
	registerField("samples", ui.samples);
	registerField("left_path", ui.left_path);
	registerField("right_path", ui.right_path);

	settings.beginGroup("camera");

	/* disable camera capturing if no cameras were selected in settings */
	if (!settings.contains("left/index") || !settings.contains("right/index")) {
		ui.source_file->setChecked(true);

		/* disable first 2 options */
		ui.source_physical->setEnabled(false);
		ui.source_virtual->setEnabled(false);
	} else {
		ui.source_physical->setChecked(true);
		ui.source_warning->setVisible(false);
	}

	settings.endGroup();
}

/* load images from files */
void CalibSetup::loadimgs(const QString &lpath, const QString &rpath)
{
	QDir leftdir(lpath);
	QDir rightdir(rpath);

	/* TODO set filter in qfiledialog */
	QStringList filesleft = leftdir.entryList(QStringList()
			<< "*.jpg" << "*.JPG" << "*.jpeg" << "*.JPG"
			<< "*.png" << "*.PNG" << "*.bmp" << "*.BMP"
			, QDir::Files);
	QStringList filesright = rightdir.entryList(QStringList()
			<< "*.jpg" << "*.JPG" << "*.jpeg" << "*.JPG"
			<< "*.png" << "*.PNG" << "*.bmp" << "*.BMP"
			, QDir::Files);

	/* TODO load by number if names are 0.jpg, 1.jpg, etc */

	/* TODO warn if not enough valid samples found */

	/* TODO check for same number of images and same file names */

	const int w = field("boardwidth").toInt();
	const int h = field("boardheight").toInt();

	for (int i = 0; i < filesleft.size(); i++) {
		std::vector<Point2f> pntsleft, pntsright;

		if (cancelimgloading) {
			imgs_left.clear();
			pnts_left.clear();

			imgs_right.clear();
			pnts_right.clear();

			return;
		}

		/* update the status display */
		emit file_load(i + 1, filesleft.size());

		Mat imgleft = imread(leftdir.path().toStdString() + "/" + filesleft.at(i).toStdString(), IMREAD_COLOR);
		Mat imgright = imread(rightdir.path().toStdString() + "/" + filesright.at(i).toStdString(), IMREAD_COLOR);

		if ((!imgleft.empty() && !corners_find(imgleft, pntsleft, w, h))
				|| (!imgright.empty() && !corners_find(imgright, pntsright, w, h)))
			continue;

		imgs_left.push_back(imgleft.clone());
		pnts_left.push_back(pntsleft);

		imgs_right.push_back(imgright.clone());
		pnts_right.push_back(pntsright);
	}
}

bool CalibSetup::validatePage()
{
	/* only execute the code below if we are loading files */
	if (!ui.source_file->isChecked())
		return true;

	/* if samples have been loaded */
	if (imgsloaded) {
		imgsloaded = false;
		return true;
	}

	/* disable all controls */
	ui.dimencontrols->setEnabled(false);
	ui.squaresizecontrols->setEnabled(false);
	ui.source_physical->setEnabled(false);
	ui.source_virtual->setEnabled(false);
	ui.source_file->setEnabled(false);
	ui.left->setEnabled(false);
	ui.right->setEnabled(false);

	/* load images in a new thread */
#if QT_VERSION >= 0x060000
	imgloading = QtConcurrent::run(&CalibSetup::loadimgs, this, ui.left_path->text(), ui.right_path->text());
#else
	imgloading = QtConcurrent::run(this, &CalibSetup::loadimgs, ui.left_path->text(), ui.right_path->text());
#endif
	ui.status->setVisible(true);
	watchimgloading.setFuture(imgloading);

	emit completeChanged();
	repaint();

	return false;
}

bool CalibSetup::isComplete() const
{
	/* only execute the code below if we are loading files */
	if (!ui.source_file->isChecked())
		return true;

	/* if samples are currently being loaded */
	if (watchimgloading.isRunning())
		return false;

	/* check if paths exists */
	if (ui.left_path->text().isEmpty() || ui.right_path->text().isEmpty())
		return false;

	QDir leftdir(ui.left_path->text());
	QDir rightdir(ui.right_path->text());

	return leftdir.exists() && rightdir.exists();
}

/* go to the appropriate page depending on which sample source is selected */
int CalibSetup::nextId() const
{
	if (ui.source_file->isChecked())
		return CalibWizard::Review;
	else
		return CalibWizard::Capture;
}

/* update autocapture interval slider label */
void CalibSetup::updateinterval(int num)
{
	ui.autocap_label2->setText(QString::number(num * AUTOCAP_MULTIPLIER) + " ms");
}

/*
 * Capture Page
 */
CalibCapture::CalibCapture(QWidget *parent) : QWizardPage(parent)
{
	ui.setupUi(this);
	setCommitPage(true);
	setButtonText(QWizard::CommitButton, tr("Submit"));

	capturetimer = new QTimer(this);
	capturetimer->setSingleShot(true);
	connect(capturetimer, &QTimer::timeout, this, &CalibCapture::live);

	autocaptimer = new QTimer(this);
	connect(autocaptimer, &QTimer::timeout, this, &CalibCapture::autocapture);

	connect(ui.capture, &QAbstractButton::clicked, this, &CalibCapture::capture);

	connect(&watchfinding, &QFutureWatcher<bool>::resultReadyAt, this,
	[=](int index)
	{
		(void) index;

		ui.status->setVisible(true);
		if (watchfinding.result()) {
			ui.status->setStyleSheet("QLabel { color: green; }");
			ui.status->setText(tr("Successfully found corners"));

			/* show corners on screen */
			Mat imgleft = imgs_left.back().clone(), imgright = imgs_right.back().clone();
			corners_draw(imgleft, pnts_left.back(),
					field("boardwidth").toInt(), field("boardheight").toInt());
			corners_draw(imgright, pnts_right.back(),
					field("boardwidth").toInt(), field("boardheight").toInt());

			ui.left->setPixmap(mattopixmap(imgleft).scaled(ui.left->size(), Qt::KeepAspectRatio));
			ui.right->setPixmap(mattopixmap(imgright).scaled(ui.right->size(), Qt::KeepAspectRatio));
		} else {
			ui.status->setStyleSheet("QLabel { color: red; }");
			ui.status->setText(tr("Failed to find corners"));
		}

		if (field("source_virtual").toBool()) {
			/* exit if this is the last image */
			if ((unsigned long int) field("samples").toInt() == imgs_left.size()) {
				wizard()->next();
				return;
			}

			/* load the next chessboard image */
			chessboard->nextPos();
		}

		/* go live again after a short timeout */
		emit completeChanged();
		capturetimer->start(CAPTURE_TIMEOUT);
		repaint();
	});
}

/* start the videocapture streams */
void CalibCapture::initializePage()
{
	int res = 1;

	/* update the active devices */
	vdev_left = left();
	vdev_right = right();

	/* open the camera streams */
	if (!vdev_left || (res = vdev_left->open()) < 0) {
		/* hide ui elements and display error message */
		ui.status->setVisible(false);
		ui.countdown->setVisible(false);
		ui.capture->setVisible(false);
		ui.left->setText(tr("unable to open camera: error ") + QString::number(res));

		return;
	}

	if (!vdev_right || (res = vdev_right->open()) < 0) {
		/* close the other device (which was successfully opened) */
		vdev_left->close();

		/* hide ui elements and display error message */
		ui.status->setVisible(false);
		ui.countdown->setVisible(false);
		ui.capture->setVisible(false);
		ui.right->setText(tr("unable to open camera: error ") + QString::number(res));

		return;
	}

	/* show the chessboard if the user selected automatic calibration */
	if (field("source_virtual").toBool()) {
		chessboard = new Chessboard;

		/* set the dimensions and squaresize selected by the user */
		chessboard->initialize(field("boardwidth").toInt(), field("boardheight").toInt(), field("squaresize").toFloat());

		chessboard->show();

		/* move to second display if this option is selected */
		if (field("secondmon").toBool())
			chessboard->windowHandle()->setScreen(((QApplication *) QApplication::instance())->screens()[1]);

		/* fullscreen window */
		chessboard->showFullScreen();

		/* raise window to front (can be undo with a mouseclick) */
		chessboard->activateWindow();
		chessboard->raise();
	}

	live();
}

void CalibCapture::cleanupPage()
{
	watchfinding.cancel();

	validatePage();

	imgs_left.clear();
	pnts_left.clear();

	imgs_right.clear();
	pnts_right.clear();

	if (field("source_virtual").toBool() && chessboard) {
		delete chessboard;
		chessboard = nullptr;
	}
}

bool CalibCapture::validatePage()
{
	autocaptimer->stop();
	capturetimer->stop();

	if (vdev_left)
		vdev_left->close();
	if (vdev_right)
		vdev_right->close();

	if (field("source_virtual").toBool() && chessboard) {
		delete chessboard;
		chessboard = nullptr;
	}

	return true;
}

bool CalibCapture::isComplete() const
{
	return imgs_left.size() >= MINSAMPLES;
}

/* start the video capturing process */
void CalibCapture::live()
{
	if (vdev_left && vdev_left->open() == 0)
		vdev_left->stream(ui.left, true, false);
	if (vdev_right && vdev_right->open() == 0)
		vdev_right->stream(ui.right, true, false);

	ui.status->setVisible(false);

	ui.index->setText(QString::number(imgs_left.size() + 1) + "/" + QString::number(MINSAMPLES));

	/* return if 1+ camera is not available and disable capturing */
	if (!vdev_left->isOpen() || !vdev_right->isOpen()) {
		ui.countdown->setVisible(false);
		ui.capture->setEnabled(false);
		return;
	}

	if (field("source_virtual").toBool()) {
		/* auto capture using virtual chessboard */
		ui.countdown->setVisible(false);
		ui.capture->setVisible(false);

		autocaptimeout = AUTOCALIB_TIMEOUT;

		autocaptimer->start(AUTOCAP_MULTIPLIER);
	} else if (field("autocap").toBool()) {
		/* auto capturing */
		ui.countdown->setVisible(true);
		ui.capture->setVisible(false);

		autocaptimeout = field("autocap_interval").toInt();
		ui.countdown->setNum(floor((double) autocaptimeout * AUTOCAP_MULTIPLIER / 1000));

		autocaptimer->start(AUTOCAP_MULTIPLIER);
	} else {
		/* manual capturing */
		ui.countdown->setVisible(false);
		ui.capture->setVisible(true);
	}
}

/* resize pixmaps on window resize */
void CalibCapture::resizeEvent(QResizeEvent *e)
{
	(void) e;

	if (vdev_left)
		vdev_left->updateSurface(ui.left, true);
	if (vdev_right)
		vdev_right->updateSurface(ui.right, true);
}

/* find the chessboard corners */
bool CalibCapture::findCorners(const int boardwidth, const int boardheight)
{
	Mat imgleft, imgright;
	std::vector<Point2f> pntsleft, pntsright;

	/* get the last frame from both video device */
	imgleft = vdev_left->getFrame();
	imgright = vdev_right->getFrame();

	if (imgleft.empty() || imgright.empty())
		return false;

	/* look for chessboard corners */
	if (!corners_find(imgleft, pntsleft, boardwidth, boardheight) ||
			!corners_find(imgright, pntsright, boardwidth, boardheight))
		return false;

	/* save the images to the buffer if the corners are detected */
	imgs_left.push_back(imgleft.clone());
	pnts_left.push_back(pntsleft);

	imgs_right.push_back(imgright.clone());
	pnts_right.push_back(pntsright);

	return true;
}

/* capture a sample */
void CalibCapture::capture()
{
	/* stop capturing */
	autocaptimer->stop();

	if (vdev_left)
		vdev_left->stop();
	if (vdev_right)
		vdev_right->stop();

	/* hide ui elements (temporarily) */
	ui.capture->setVisible(false);
	ui.countdown->setVisible(false);

	/* look for chessboard corners in a new thread */
	ui.status->setVisible(true);
	ui.status->setStyleSheet("");
	ui.status->setText(tr("Detecting corners..."));
#if QT_VERSION >= 0x060000
	finding = QtConcurrent::run(&CalibCapture::findCorners, this, field("boardwidth").toInt(), field("boardheight").toInt());
#else
	finding = QtConcurrent::run(this, &CalibCapture::findCorners, field("boardwidth").toInt(), field("boardheight").toInt());
#endif
	watchfinding.setFuture(finding);
}

/* update autocaptimeout or capture if timeout has expired */
void CalibCapture::autocapture()
{
	double sec = (double) --autocaptimeout * AUTOCAP_MULTIPLIER / 1000;

	/* check if we're doing fully automatic calibration or manual */
	if (!field("source_virtual").toBool()) {
		/* only update screen with whole seconds */
		if (floor(sec) == sec)
			ui.countdown->setNum(sec);
		/* else if (sec == (double) AUTOCAP_MULTIPLIER / 1000)
			ui.countdown->setNum(0); */
	}

	/* capture when timeout has expired */
	if (!autocaptimeout)
		capture();
}

/*
 * Review Page
 */
CalibReview::CalibReview(QWidget *parent) : QWizardPage(parent)
{
	ui.setupUi(this);

	connect(ui.prev, &QAbstractButton::clicked, this,
	[=]()
	{
		cycle(-1);
	});
	connect(ui.next, &QAbstractButton::clicked, this,
	[=]()
	{
		cycle(1);
	});
	connect(ui.gridlines, &QCheckBox::stateChanged, this,
	[=]()
	{
		cycle(0);
	});

	connect(ui.discard, &QAbstractButton::clicked, this, &CalibReview::discard);

	connect(ui.exportall, &QAbstractButton::clicked, this,
	[=]()
	{
		std::vector<int> param;
		param.push_back(IMWRITE_JPEG_QUALITY);
		param.push_back(95);

		QFileDialog fd(this);
		fd.setWindowTitle(tr("Export all to directory"));
		fd.setFileMode(QFileDialog::Directory);

		if (!fd.exec() || fd.selectedFiles()[0].isEmpty())
			return;

		QDir dir(fd.selectedFiles()[0]);

		/* create a 'left/' and 'right/' folder */
		if (!dir.mkdir(tr("left")) || !dir.mkdir(tr("right"))) {
			QMessageBox::warning(this, "Babymeter 3",
					tr("Export failed because a left/ and right/ directory couldn't be created."));

			return;
		}

		/* FIXME export on a separate thread to prevent UI from hanging (for a very short time) */

		/* export all images to a 'left/' and 'right/' folder */
		for (unsigned int i = 0; i < imgs_left.size(); i++) {
			imwrite(dir.path().toStdString() + "/left/" + QString::number(i).toStdString() + ".jpg",
					imgs_left[i], param);
			imwrite(dir.path().toStdString() + "/right/" + QString::number(i).toStdString() + ".jpg",
					imgs_right[i], param);
		}

		/* export squaresize and board dimensions to an ini file */
		QSettings file(dir.path() + "/properties.ini", QSettings::IniFormat);

		/* remove all old settings if present */
		for (auto &key : file.allKeys())
			file.remove(key);

		file.setValue("squaresize", field("squaresize").toFloat());
		file.setValue("boardwidth", field("boardwidth").toInt());
		file.setValue("boardheight", field("boardheight").toInt());

		QMessageBox::information(this, "Babymeter 3", tr("Successfully exported ") +
				QString::number(imgs_left.size()) + tr(" file(s)"));
	});

	connect(&watchcalibrating, &QFutureWatcher<void>::finished, this,
	[=]()
	{
		calibrated = true;
		emit completeChanged();

		/* display a dialog to show the calibration has been completed */
		QMessageBox::information(this, tr("Babymeter 3"),
				tr("The calibration was successful"), QMessageBox::Ok);

		wizard()->close();
	});

	/* file loading status text */
	ui.status->setVisible(false);
	connect(this, &CalibReview::calibrate_status, this,
	[=](QString str)
	{
		ui.status->setText(str);
	});
}

void CalibReview::initializePage()
{
	if (field("source_file").toBool()) {
		ui.label1->setVisible(false);
		ui.label2->setVisible(true);
		ui.discard->setText(tr("Exclude"));
		ui.exportall->setVisible(false);
	} else {
		ui.label1->setVisible(true);
		ui.label2->setVisible(false);
		ui.exportall->setVisible(true);
	}

	index = 0;
}

void CalibReview::showEvent(QShowEvent *ev)
{
	QWizardPage::showEvent(ev);
	cycle(0);
}

/* resize pixmaps on window resize */
void CalibReview::resizeEvent(QResizeEvent *e)
{
	(void) e;

	cycle(0);
}

void CalibReview::cleanupPage()
{
	/* if (!field("source").toBool())
		return; */

	imgs_left.clear();
	pnts_left.clear();

	imgs_right.clear();
	pnts_right.clear();
}

/* perform calibration, save changes and exit calibration */
void CalibReview::calibrate(const int boardwidth, const int boardheight, const float squaresize)
{
	const Size size = imgs_left[0].size();

	/*
	 * 1.
	 * assemble array of object points
	 */
	std::vector<std::vector<Point3f>> obj;

	for (unsigned int i = 0; i < imgs_left.size(); i++)
		obj.push_back(getobj(boardwidth, boardheight, squaresize));

	/*
	 * 2.
	 * calibrate left and right camera
	 */
	Mat camleft, camright;			/* camera matrices */
	Mat disleft, disright;			/* distortion coefficients */
	std::vector<Mat> rvecsleft, rvecsright;	/* rotation vectors */
	std::vector<Mat> tvecsleft, tvecsright;	/* translation vectors */

	emit calibrate_status(tr("Calibrating cameras..."));
	const int flags = CALIB_FIX_K4 | CALIB_FIX_K5;
	calibrateCamera(obj, pnts_left, size, camleft, disleft, rvecsleft, tvecsleft, flags);
	calibrateCamera(obj, pnts_right, size, camright, disright, rvecsright, tvecsright, flags);

	/*
	 * 3.
	 * calibrate the cameras for stereoscopy
	 */
	Mat rot;	/* rotation matrix */
	Vec3d trans;	/* translation vector */
	Mat ess;	/* essential matrix */
	Mat fun;	/* fundamental matrix */
	double error;	/* reprojection error */

	emit calibrate_status(tr("Calculating stereoscopic parameters..."));
	error = stereoCalibrate(obj, pnts_left, pnts_right, camleft, disleft,
			camright, disright, size, rot, trans, ess, fun);

	/*
	 * 4.
	 * compute the rectification transformations
	 */
	Mat rotleft, rotright;	/* rotation matrices */
	Mat proleft, proright;	/* projection matrices */
	Mat dtdmap;		/* disparity-to-depth mapping matrix */

	emit calibrate_status(tr("Rectifying stereoscopic parameters..."));
	stereoRectify(camleft, disleft, camright, disright, size, rot, trans,
			rotleft, rotright, proleft, proright, dtdmap);

	/* not needed right now, perhaps in the future */
#if 0
	/*
	 * 5.
	 * compute the undistortion and rectification transformation map
	 */
	Mat mapxleft, mapyleft;		/* left camera maps */
	Mat mapxright, mapyright;	/* right camera maps */

	initUndistortRectifyMap(camleft, disleft, rotleft, proleft, size,
			CV_32F, mapxleft, mapyleft);
	initUndistortRectifyMap(camright, disright, rotright, proright, size,
			CV_32F, mapxright, mapyright);
#endif

	/*
	 * save all chessboard and calibration data to this location:
	 * - Linux: ~/.config/babymeter/babymeter3.conf
	 * - macOS: [Application Home Directory]/Settings/babymeter/babymeter3
	 * - Windows: HKCU\Software\babymeter\babymeter3
	 */
	settings.beginGroup("calibration");
	settings.setValue("date", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
	settings.setValue("error", error);
	settings.setValue("squaresize", QString::number(squaresize));
	settings.setValue("boardwidth", QString::number(boardwidth));
	settings.setValue("boardheight", QString::number(boardheight));
	settings.setValue("camera_left", mattostring(camleft));
	settings.setValue("camera_right", mattostring(camright));
	settings.setValue("distortion_left", mattostring(disleft));
	settings.setValue("distortion_right", mattostring(disright));
	settings.setValue("rotation_left", mattostring(rotleft));
	settings.setValue("rotation_right", mattostring(rotright));
	settings.setValue("projection_left", mattostring(proleft));
	settings.setValue("projection_right", mattostring(proright));
	settings.setValue("rotation", mattostring(rot));
	settings.setValue("translation", mattostring((Mat) trans));

	/* not needed right now, perhaps in the future */
#if 0
	settings.setValue("essential",
			QString::fromStdString(mattostring(ess)));
	settings.setValue("fundamental",
			QString::fromStdString(mattostring(fun)));
	settings.setValue("mapx_left",
			QString::fromStdString(mattostring(mapxleft)));
	settings.setValue("mapy_left",
			QString::fromStdString(mattostring(mapyleft)));
	settings.setValue("mapx_right",
			QString::fromStdString(mattostring(mapxright)));
	settings.setValue("mapy_right",
			QString::fromStdString(mattostring(mapyright)));
#endif
	settings.endGroup();

	emit calibrate_status(tr("Done"));
}

bool CalibReview::validatePage()
{
	if (calibrated)
		return true;

	/* run calibration in a new thread */
#if QT_VERSION >= 0x060000
	calibrating = QtConcurrent::run(&CalibReview::calibrate, this,
			field("boardwidth").toInt(), field("boardheight").toInt(), field("squaresize").toFloat());
#else
	calibrating = QtConcurrent::run(this, &CalibReview::calibrate,
			field("boardwidth").toInt(), field("boardheight").toInt(), field("squaresize").toFloat());
#endif
	ui.status->setVisible(true);
	watchcalibrating.setFuture(calibrating);

	/* disable all controls */
	ui.prev->setEnabled(false);
	ui.next->setEnabled(false);
	ui.discard->setEnabled(false);
	ui.label1->setEnabled(false);
	ui.label2->setEnabled(false);
	ui.gridlines->setVisible(false);
	ui.exportall->setVisible(false);

	emit completeChanged();
	repaint();

	return false;
}

bool CalibReview::isComplete() const
{
	/* grey out if calibration is running */
	if (watchcalibrating.isRunning())
		return false;

	return imgs_left.size() >= MINSAMPLES;
}

/* next/previous/refresh the image and points buffers */
void CalibReview::cycle(int dir)
{
	const int w = field("boardwidth").toInt();
	const int h = field("boardheight").toInt();

	/* which direction to move in: -1 (left), 1 (right) or 0 (refresh) */
	if (dir < 0 && index > 0)
		index--;
	else if (dir > 0 && index < imgs_left.size() - 1)
		index++;
	else if (dir)
		return;

	/* disable prev/next button accordingly */
	ui.prev->setEnabled((index > 0));
	ui.next->setEnabled((index < imgs_left.size() - 1));

	/* disable the delete button if this is the last sample in the buffer */
	if (!watchcalibrating.isRunning() && !calibrated)
		ui.discard->setDisabled((imgs_left.size() == 1));

	ui.index->setText(QString::number(index + 1) + "/" + QString::number(imgs_left.size()));

	/* make copy of the img in case it is modified */
	Mat leftbuf, rightbuf;
	leftbuf = imgs_left[index].clone();
	rightbuf = imgs_right[index].clone();

	/* draw corners if checked */
	if (ui.gridlines->isChecked()) {
		corners_draw(imgs_left[index], pnts_left[index], w, h);
		corners_draw(imgs_right[index], pnts_right[index], w, h);
	}

	QPixmap pixleft = mattopixmap(imgs_left[index]);
	ui.left->setPixmap(pixleft.scaled(ui.left->size(),
				Qt::KeepAspectRatio));

	QPixmap pixright = mattopixmap(imgs_right[index]);
	ui.right->setPixmap(pixright.scaled(ui.right->size(), Qt::KeepAspectRatio));

	/* restore image without modifications */
	imgs_left[index] = leftbuf;
	imgs_right[index] = rightbuf;

	repaint();
}

/* delete the currently displayed sample from the buffers */
void CalibReview::discard()
{
	imgs_left.erase(imgs_left.begin() + index);
	pnts_left.erase(pnts_left.begin() + index);
	imgs_right.erase(imgs_right.begin() + index);
	pnts_right.erase(pnts_right.begin() + index);

	/* decrement index if this is the tail sample in the buffer */
	while (index + 1 > imgs_left.size())
		index--;

	/*
	 * disable the finish button if the amount of samples is less than
	 * the minimum
	 */
	emit completeChanged();

	/* refresh */
	cycle(0);
}
