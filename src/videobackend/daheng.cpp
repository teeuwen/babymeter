/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 - 2021 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 - 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef DAHENG

#include <videobackend/daheng.h>

#include <DxImageProc.h>

#include <QDebug> /* XXX TEMP XXX */

DahengAcquisitionThread::~DahengAcquisitionThread()
{
	delete[] rawframe;
	delete[] rgbframe;
}

void DahengAcquisitionThread::setDeviceHandle(GX_DEV_HANDLE dev)
{
	handle = dev;

	/* FIXME temporary hack because the resolution is reset to 640x480 every time for no apprarent reason */
	GXSetInt(handle, GX_INT_WIDTH, 3088);
	GXSetInt(handle, GX_INT_HEIGHT, 2064);

	/* load parameters from camera into memory */
	if (GXGetInt(handle, GX_INT_PAYLOAD_SIZE, &payloadsize) != GX_STATUS_SUCCESS)
		qDebug() << "gxiapi: loading payload size info failed";
	if (GXGetInt(handle, GX_INT_WIDTH, &width) != GX_STATUS_SUCCESS)
		qDebug() << "gxiapi: loading image width info failed";
	if (GXGetInt(handle, GX_INT_HEIGHT, &height) != GX_STATUS_SUCCESS)
		qDebug() << "gxiapi: loading image height info failed";
	if (GXGetEnum(handle, GX_ENUM_PIXEL_COLOR_FILTER, &colorfilter) != GX_STATUS_SUCCESS)
		qDebug() << "gxiapi: loading pixel color filter info failed";

	/* allocate memory for the RAW and RGB image buffers */
	rawframe = new char[payloadsize];
	rgbframe = new char[width * height * 3];
}

void __stdcall DahengAcquisitionThread::callback(GX_FRAME_CALLBACK_PARAM *params)
{
	DahengAcquisitionThread *thread = (DahengAcquisitionThread *) params->pUserParam;
	if (params->status != GX_FRAME_STATUS_SUCCESS)
		return;

	/* copy the image buffer into a separate buffer */
	memcpy(thread->rawframe, params->pImgBuf, params->nImgSize);

	/* convert the raw image to RGB */
	DxRaw8toRGB24(thread->rawframe, thread->rgbframe,
			(VxUint32) thread->width, (VxUint32) thread->height,
			RAW2RGB_NEIGHBOUR, DX_PIXEL_COLOR_FILTER(thread->colorfilter), true);

	/* and copy the RGB buffer into a cv::Mat object */
	cv::Mat img(thread->height, thread->width, CV_8UC3);
	memcpy(img.data, thread->rgbframe, thread->height * thread->width * 3);

	/* flip the image vertically (because for some reason it comes in upside down) */
	cv::flip(img, img, 0);

	thread->mutex.lock();
	thread->frame = img;
	thread->mutex.unlock();
}

void DahengAcquisitionThread::run()
{
	/* register our acquisition callback function and start */
	GXRegisterCaptureCallback(handle, this, callback);
	GXSendCommand(handle, GX_COMMAND_ACQUISITION_START);

	/* loop to keep the thread alive (we may not even need threads on win32) */
	while (surfacecount);

	/* stop acquisition and unregister our callback function */
	GXSendCommand(handle, GX_COMMAND_ACQUISITION_STOP);
	GXUnregisterCaptureCallback(handle);
}

DahengVideoDevice::DahengVideoDevice(const char *friendlyname, char *sn)
{
	qDebug() << "gxapi: found device: " << friendlyname << sn;
	name = QString(friendlyname);
	memcpy(serial, sn, GX_INFO_LENGTH_32_BYTE);
}

int DahengVideoDevice::open()
{
	GX_OPEN_PARAM params;
	GX_STATUS res;

	/* open the device only if it wasn't already */
	if (isOpen())
		return 0;

	params.accessMode = GX_ACCESS_EXCLUSIVE;
	params.openMode = GX_OPEN_SN;
	params.pszContent = serial;

	qDebug() << "gxapi: opening device" << serial;

	/* open the device */
	if ((res = GXOpenDevice(&params, &handle)) != GX_STATUS_SUCCESS) {
		qDebug() << "gxapi: error opening device" << res;
		return res;
	}

	opened = true;

	return 0;
}
int DahengVideoDevice::startAcq()
{
	if (acqthread)
		return 0;

	/* start a separate thread for image acquisition and processing */
	if (!(acqthread = new DahengAcquisitionThread(this)))
		return -1;
	((DahengAcquisitionThread *) acqthread)->setDeviceHandle(handle);
	acqthread->start();

	return 1;
}

void DahengVideoDevice::stopAcq()
{
	if (!acqthread)
		return;

	acqthread->surfacecount = 0;
	acqthread->exit();
	acqthread->wait();

	delete acqthread;
	acqthread = nullptr;

	return;
}

void DahengVideoDevice::close()
{
	GX_STATUS res;

	/* don't attempt to close if this device isn't open, this crashes the GxIAPI */
	if (!isOpen())
		return;

	/* stop acquisition and drawing to surfaces first */
	stop();

	if ((res = GXCloseDevice(handle)) != GX_STATUS_SUCCESS)
		qDebug() << "gxapi: error closing device" << res;

	opened = false;

	qDebug() << "gxapi: device is closed";
}

bool DahengVideoDevice::isOpen()
{
	return opened;
}

void DahengVideoDevice::loadFrame()
{
	if (isOpen())
		image = acqthread->getFrame();
}

DahengVideoDeviceManager::DahengVideoDeviceManager()
{
	uint32_t numdevices;
	size_t devinfosize;
	GX_DEVICE_BASE_INFO *devinfo;

	GXInitLib();

	GXUpdateDeviceList(&numdevices, 1000);
	qDebug() << numdevices << "gxapi: devices found";

	/* query details for all devices */
	devinfosize = numdevices * sizeof(GX_DEVICE_BASE_INFO);
	devinfo = new GX_DEVICE_BASE_INFO[numdevices];
	GXGetAllDeviceBaseInfo(devinfo, &devinfosize);

	for (size_t i = 0; i < numdevices; i++)
		devices.append((VideoDevice *) new DahengVideoDevice(devinfo[i].szDisplayName, devinfo[i].szSN));

	delete[] devinfo;
}

DahengVideoDeviceManager::~DahengVideoDeviceManager()
{
	GXCloseLib();
}

QList<VideoDevice *> DahengVideoDeviceManager::enumerate()
{
	return devices;
}

#endif
